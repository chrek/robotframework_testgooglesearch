*** Settings ***
Documentation     A test suite with a single test for valid login.
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot


*** Test Cases ***
TC - Open Browser
    Open Browser To Google Page

TC - Search on Google
    Search Topic    BrowserStack
