*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary
Resource    googlesearch_testcase.robot

*** Variables ***
${HOMEPAGE}       https://www.google.com
${BROWSER}        Chrome

*** Keywords ***
Open Browser To Google Page
    Open Browser    ${HOMEPAGE}    ${BROWSER}
    Maximize Browser Window
    Google Page Should Be Open

Google Page Should Be Open
    Title Should Be    Google

Input Username
    [Arguments]    ${username}
    Input Text    username_field    ${username}

Search Topic
    [Arguments]    ${topic}
    Input Text    name=q    ${topic}
    Press Keys     name=q    \\13
 