====================================================
Web testing with Robot Framework and SeleniumLibrary
====================================================

This project (Data driven test cases on web testing) is using `Robot Framework` 
as the test automation framework and SeleniumLibrary as the test library
Test logs and reports are generated at the end of test execution

Python Virtual Environment
==========================

Installation of the packages are made in a virtual Environment
The Browser Drivers are all provided in the Script folder of Python
Interpreter (python.exe)) location of the virtual environment

Python
Robot Framework
SeleniumLibrary
All the required packages



